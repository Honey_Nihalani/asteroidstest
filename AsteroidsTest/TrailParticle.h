#ifndef TRAIL_PARTICLE_H_INCLUDED
#define TRAIL_PARTICLE_H_INCLUDED

#include "GameEntity.h"
#include "Particle.h"
#include <list>

class TrailParticle : public GameEntity
{
public:
	TrailParticle(D3DXVECTOR3 position, D3DXVECTOR3 direction);
	void SetDirection(D3DXVECTOR3 direction);
	void SetControlInput(float acceleration);
	void Update(System *system);
	void Render(Graphics *graphics);
private:
	D3DXVECTOR3 velocity_, direction_;
	typedef std::list<Particle*> ParticleList;
	ParticleList particles_;
};

#endif
