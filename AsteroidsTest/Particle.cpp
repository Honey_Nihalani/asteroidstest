#include "Particle.h"
#include "Graphics.h"
#include "Random.h"

Particle::Particle(D3DXVECTOR3 position, D3DXVECTOR3 direction, float speed, float lifespan, D3DCOLOR color)
{
	Reset(position, direction, speed, lifespan);
	particle_.z = 100.0f;
	particle_.diffuse = color;
}

void Particle::SetControlInput(float acceleration)
{
	const float VELOCITY_TWEEN = 0.05f;

	D3DXVECTOR3 idealVelocity;
	D3DXVec3Scale(&idealVelocity, &direction_, acceleration * maxSpeed_);
	D3DXVec3Lerp(&velocity_, &velocity_, &idealVelocity, VELOCITY_TWEEN);
}

void Particle::Update(System *system)
{
	lifespan_--;

	D3DXVECTOR3 position = GetPosition();
	D3DXVec3Add(&position, &position, &velocity_);
	SetPosition(position);

	particle_.x = position.x;
	particle_.y = position.y;
}

void Particle::Render(Graphics* graphics)
{
	DWORD starFvf = D3DFVF_XYZ | D3DFVF_DIFFUSE;
	graphics->SetVertexFormat(starFvf);
	graphics->SetPointSize(2.0f);
	graphics->DisableLighting();
	graphics->DrawImmediate(D3DPT_POINTLIST,
		1,
		&particle_,
		sizeof(particle_));
	graphics->EnableLighting();
}

bool Particle::isTimeout()
{
	if (lifespan_ < 0)
		return true;
	return false;
}

void Particle::Reset(D3DXVECTOR3 position, D3DXVECTOR3 direction, float speed, float lifespan)
{
	maxSpeed_ = speed + 2.0f;
	lifespan_ = lifespan;
	SetPosition(position);
	D3DXVECTOR3 normalisedDirection;
	D3DXVec3Normalize(&normalisedDirection, &direction);
	direction_ = normalisedDirection;
	velocity_ = normalisedDirection * speed;
	particle_.x = position.x;
	particle_.y = position.y;
}