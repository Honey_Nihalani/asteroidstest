#include "PlayingState.h"
#include "System.h"
#include "Game.h"
#include "Font.h"
#include "Graphics.h"

PlayingState::PlayingState()
	: font_(0)
{
}

PlayingState::~PlayingState()
{
}

void PlayingState::OnActivate(System *system, StateArgumentMap &args)
{
	Game *game = system->GetGame();

	level_ = args["Level"].asInt;
	game->InitialiseLevel(level_);
	if (font_ == 0)
	{
		font_ = system->GetGraphics()->CreateXFont("Arial", 24);
	}
}

void PlayingState::OnUpdate(System *system)
{
	Game *game = system->GetGame();
	game->Update(system);

	if (game->IsLevelComplete())
	{
		StateArgumentMap args;
		args["Level"].asInt = level_ + 1;
		system->SetNextState("LevelStart", args);
	}
	else if (game->IsGameOver())
	{
		system->SetNextState("GameOver");
	}
}

void PlayingState::OnRender(System *system)
{
	Game *game = system->GetGame();
	game->RenderEverything(system->GetGraphics());

	std::string str = "Score : " + std::to_string(game->getScore());
	if (font_)
	{
		font_->DrawText(str, 50, 50, 0xffffff00);
	}
}

void PlayingState::OnDeactivate(System *system)
{
}
