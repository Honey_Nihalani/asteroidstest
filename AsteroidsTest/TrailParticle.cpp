#include "TrailParticle.h"
#include "Graphics.h"
#include "Random.h"

TrailParticle::TrailParticle(D3DXVECTOR3 position, D3DXVECTOR3 direction)
{
	SetPosition(position);
	direction_ = direction;
	for (int i = 0; i < 30; i++)
	{
		float lifespan = Random::GetFloat(18, 22);
		float speed = Random::GetFloat(3.0, 5.0);
		float rand_angle = Random::GetFloat(-1, 1);

		direction_.x += (sin(rand_angle) * 0.5);
		direction_.y += (cos(rand_angle) * 0.5);
		direction_.x *= -1;
		direction_.y *= -1;
		direction_.z *= -1;

		Particle* particle = new Particle(position, direction_, speed, lifespan, 0xffff0000);
		particles_.push_back(particle);
	}
}

void TrailParticle::SetControlInput(float acceleration)
{
	if (acceleration < 0)
	{
		acceleration = -0.1;
	}
	for (Particle* particle : particles_)
	{
		particle->SetControlInput(acceleration);
	}
}

void TrailParticle::SetDirection(D3DXVECTOR3 direction)
{
	direction_ = direction;
}

void TrailParticle::Update(System *system)
{
	for (Particle* particle : particles_)
	{
		particle->Update(system);
		if (particle->isTimeout())
		{
			D3DXVECTOR3 direction = direction_;

			float lifespan = Random::GetFloat(18, 22);
			float speed = Random::GetFloat(3.0, 5.0);
			float rand_angle = Random::GetFloat(-1, 1);

			direction.x += (sin(rand_angle) * 0.5);
			direction.y += (cos(rand_angle) * 0.5);
			direction.x *= -1;
			direction.y *= -1;
			direction.z *= -1;

			particle->Reset(GetPosition(), direction, speed, lifespan);
		}
	}
}

void TrailParticle::Render(Graphics *graphics)
{
	for (Particle* p : particles_)
	{
		p->Render(graphics);
	}
}