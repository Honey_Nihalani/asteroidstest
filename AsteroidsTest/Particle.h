#ifndef PARTICLE_H_INCLUDE
#define PARTICLE_H_INCLUDE

#include "GameEntity.h"

class Particle : public GameEntity
{
public:
	Particle(D3DXVECTOR3 position, D3DXVECTOR3 direction, float speed, float lifespan, D3DCOLOR color);
	void Reset(D3DXVECTOR3 position, D3DXVECTOR3 direction, float speed, float lifespan);
	bool isTimeout();
	void SetControlInput(float acceleration);

	void Update(System *system);
	void Render(Graphics *graphics);
private:
	float maxSpeed_;
	int lifespan_;
	D3DXVECTOR3 velocity_, direction_;
	struct Particle_struct
	{
		float x, y, z;
		D3DCOLOR diffuse;
	};
	Particle_struct particle_;
};

#endif