#pragma once
#ifndef GAMEENTITIES_H_INCLUDED
#define GAMEENTITIES_H_INCLUDED

enum class GameEntities : char
{
	Asteroid,
	Background,
	Bullet,
	Explosion,
	Ship
};

#endif // GAMEENTITIES_H_INCLUDED
