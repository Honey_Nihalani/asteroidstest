#ifndef BLAST_PARTICLE_H_INCLUDED
#define BLAST_PARTICLE_H_INCLUDED

#include "GameEntity.h"
#include "Particle.h"
#include <list>

class BlastParticle : public GameEntity
{
public:
	BlastParticle(D3DXVECTOR3 position, int size);
	void Update(System *system);
	void Render(Graphics *graphics);
 	inline bool isFxOver() { return fxOver_; };

private:
	bool fxOver_;
	D3DXVECTOR3 velocity_, direction_;
	typedef std::list<Particle*> ParticleList;
	ParticleList particles_;
};

#endif