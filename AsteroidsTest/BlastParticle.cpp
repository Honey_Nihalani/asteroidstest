#include "BlastParticle.h"
#include "Graphics.h"
#include "Random.h"

BlastParticle::BlastParticle(D3DXVECTOR3 position, int size)
	: fxOver_(false)
{
	// 1 small
	// 2 medium
	// 3 large

	D3DXVECTOR3 direction = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	SetPosition(position);
	for (int i = 0; i < 20 * size; i++)
	{
		float lifespan = Random::GetFloat(30, 32);
		float speed = Random::GetFloat(1.0, 1.5);
		lifespan += (size * 2);
		float rand_radius_x = Random::GetFloat(-1, 1);
		float rand_radius_y = Random::GetFloat(-1, 1);
		direction.x = rand_radius_x;
		direction.y = rand_radius_y;

		Particle* particle = new Particle(position, direction, speed, lifespan, 0xffff00ff);
		particles_.push_back(particle);
	}
}

void BlastParticle::Update(System *system)
{
	for (Particle* particle : particles_)
	{
		if (particle->isTimeout())
		{
			fxOver_ = true;
			particles_.remove(particle);
			delete particle;
			break;
		}
		else
		{
			particle->Update(system);
		}
	}
}

void BlastParticle::Render(Graphics *graphics)
{
	for (Particle* particle : particles_)
	{
		particle->Render(graphics);
	}
}